<h1>Digit Recognizer</h1>

Dans le cadre du cours de Programmation Parallèle de l'Université de Perpignan Via Dominitia (UPVD), il est demandé de réaliser un projet permettant d'appliquer, sur un jeu de donnée comportant au moins 10 000 entrées, un algorithme de k-means (ou adapté) sur plusieurs machines afin d'optimiser les temps de calcul. <br>

<b>Auteur</b> : Pierre-Vincent VROT <br>
<b>Date</b> : 2015-01-14 <br>
<b>Sources</b> : <a href="https://gitlab.com/PapyPev/digit-reco">https://gitlab.com/PapyPev/digit-reco</a>

[![Digit Reco](http://i68.servimg.com/u/f68/09/03/21/71/screen10.jpg)](https://www.youtube.com/embed/cpoZfy_j9vQ "Digit Reco - kmeans and mpi")

<i>Vidéo sur Youtube :</i> <a href="https://youtu.be/cpoZfy_j9vQ">https://youtu.be/cpoZfy_j9vQ</a>

<!-- ====================================================================== -->
<h2>Sommaire</h2>
<ol>
    <li><a href="#contexte">Contexte</a></li>
    <li><a href="#donnees">Données</a></li>
    <li><a href="structure">Structure</a></li>
    <li><a href="#librairies">Librairies</a></li>
    <li><a href="#utilisation">Utilisation</a></li>
    <li><a href="#todo">TODO list</a></li>
</ol>

<!-- ====================================================================== -->
<h2 id="contexte">1. Contexte</h2>

Le projet consiste à classer les chiffres manuscrits en utilisant les données du MNIST (Modified National Institute of Standards and Technology). Prendre une image d'un seul chiffre manuscrit, et déterminer quel est ce chiffre. Les étapes de réalisations sont les suivantes :
<ol>
    <li>Charger les données</li>
    <li>Afficher une image issue des données</li>
    <li>Appliquer un premier algorithme k-means pour chaque image
        <ol>
            <li>Générer des centroides aléatoires</li>
            <li>Déterminer la distance entre chaque point et les centroides</li>
            <li>Associer chaque point à son centroide le plus proche</li>
            <li>Recalculer la position des centroides en fonction de la moyenne des distances entre chaque point qui lui a été associé</li>
            <li>Optimiser les centroids (ré-associer les points, recalculer la moyenne, etc..) jusqu'à être inferieur à un seuil d'acceptation</li>
            <li>Sauver les centroides générés</li>
        </ol>
    </li>
    <li>Appliquer un second algorithme k-means sur les centroids sauvés
        <ol>
            <li>Cette étape permet de centrer les recherches vers un tracé plus précis</li>
        </ol>
    </li>
    <li>Afficher un schéma des centroides groupés et des nouveaux centroides générés après le second algorithme</li>
    <li>Tracé le schéma de reconnaissance entre les nouveaux centroides</li>
    <li>Charger les nouvelles images à déterminer</li>
    <li>Appliquer un algorithme k-means sur les nouvelles images pour récupérer les centroides</li>
    <li>Déterminer à quel chiffre est associé chacune des nouvelles images</li>
</ol>

<!-- ====================================================================== -->
<h2 id="donnees">2. Données</h2>

Source : <a href="https://www.kaggle.com/c/digit-recognizer/data">https://www.kaggle.com/c/digit-recognizer/data</a><br>
Plus d’info : <a href="http://yann.lecun.com/exdb/mnist/index.html">http://yann.lecun.com/exdb/mnist/index.html</a><br>

Deux fichiers CSV par séparation à virgule sont disponibles : 
<ul>
    <li><code>train.csv</code> qui contient les données d'apprentissage</li>
    <li><code>test.csv</code> qui contient les données à déterminer</li>
</ul>

Chaque ligne du fichier représente une image en 28 pixels de large par 28 pixels de hauteur, soit 784 pixels par entrée. Les fichiers dispose de 784 colonnes nommée <code>pixel0</code> à <code>pixel783</code> renseignant la valeur en nuance de gris du pixel (de 0 à 254 inclu). A la différence du jeu d'image à déterminer, les données d'apprentissage disposent d'un champ supplémentaire nommmé <code>label</code> contenant le chiffre qui a été écrit sur l'image.

<!-- ====================================================================== -->
<h2 id="structure">3. Structure du code</h2>

<pre><code>/img-reco
|
|-- /data           : Répertoire contenant les données
|    +-- *.csv      : Fichiers de données d'apprentissage et de reconnaissance
|
|-- /docs           : Répertoire contenant la documentation technique
|    +-- *.html     : Fichiers contenant la documentation
|
+-- /src            : Répertoire contenant les sources du projet
     |-- /classes   : Contient les classes nécessaires
     |   +-- *.py   : Sources des classes
     |
     + -- main.py   : Fichier a executer pour démarer le projet
</code></pre>

<!-- ====================================================================== -->

<h2 id="librairies">4. Librairies</h2>

Pour que le projet puisse correctement fonctionner, il est nécessaire de disposer des bibliothèque Python suivantes : 
<ul>
    <li>time : Fonctions de temps de la machine
    <li>sys : Spécificités de la machine
    <li>csv : Lecture et Ecriture de CSV
    <li>getopt : Gestion des paramètres
    <li>Tkinter : Interface graphique
    <li>numpy : Gestion des ressources partagées
    <li>random : Génération des aléas
    <li>math : Fonctions mathématiques
    <li>mpi4py : Controleur de message inter-machines (MPI)
</ul>

<!-- ====================================================================== -->

<h2 id="utilisation">5. Utilisation</h2>

<b>Note : Sur les vidéos, le fichier <code>./main.py</code> est l'ancienne version du fichier <code>./default.py</code></b>

Le projet propose 4 modes d'execution : avec/sans MPI et avec/sans paramètres par défauts : 

<i>Paramètres par défaut, sans MPI :</i><br>
<b>$</b> <code>./default</code><br>

<i>Paramètres par l'utilisateur, sans MPI :</i><br>
<b>$</b> <code>default.py -i [inputfile] -s [separator] -n [number] -c [clusters] -o [outputfile]</code><br><br>

<i>Paramètres par defaut, avec MPI :</i><br>
<b>$</b> <code>mpirun -n [process] ./mpi.py</code>

<!-- ====================================================================== -->

<h2 id="todo">6. TODO list</h2>

<ul>
    <li>MPI</li>
    <ul>
        <li>Optimiser le chargement des Images initiales par process</li>
        <li>Charger sur plusieurs machines plutôt que sur des process, voir fichier hostfile</li>
        <li>Editer la récupération des paramètres pour ne pas confondre <code>-n</code> entre le nombre de process et le chiffre à tester</li>
    </ul>
    <li>Général</li>
    <ul>
        <li>Ne pas se contenter que d'un seul chiffre à tester, mais tous</li>
        <li>Revoir la fonction de traçage de schéma (très aléatoire actuellement)</li>
        <li>Lire le fichier de test pour reconnaitre les chiffres</li>
        <li>Logger dans le répertoire de sortie</li>
    </ul>
</ul>
