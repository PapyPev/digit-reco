#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
The MIT License (MIT)

Copyright (c) 2015 Pierre Vrot
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software 
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS 
IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
AND NONINFRINGEMENT. IN NO EVENT SHALL

THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.

How to use it ?
    $ python ./default -h

Data Sources from : 
    https://www.kaggle.com/c/digit-recognizer/data

Library documentation :
    getopt  : https://docs.python.org/2/library/getopt.html
    csv     : https://docs.python.org/2/library/csv.html
    tkinter : https://docs.python.org/2/library/tkinter.html
    numpy   : http://docs.scipy.org/doc/

Help Sources : 
    http://mnemstudio.org/clustering-k-means-example-1.htm

"""

###############################################################################

__author__ = 'pev'
__version__ = "1.6"
__email__ = "pev@gmx.fr"

###############################################################################

# Librairies
import time         # Get time compute script
import sys          # System library
import csv          # I/O CSV library
import getopt       # Arguments library
from Tkinter import Tk, Canvas, Frame, BOTH
import numpy as np  # Scientific computing library
import random       # Random library
import math         # Mathematicl library
import mpi4py.MPI as MPI # MPI Library

# From own classes
from classes.image import *
from classes.points import *
from classes.gui_img import *
from classes.gui_plot import *

###############################################################################

# Default values
DEF_INPUT = '../data/train.csv'
DEF_SEPAR = ','
DEF_NUMBR = 3
DEF_CLUST = 8
DEF_PRCSS = 4
DEF_OUTPU = '../res/log.txt'

###############################################################################

# Window and Image values
W_WIDTH = 400
W_HEIGHT = 400
IMG_ROWS = 28
IMG_COLS = 28
SCALE_X = 30
SCALE_Y = 30

# Parameters values
G_INPUT = ''
G_SEPAR = ''
G_NUMBR = 0
G_CLUST = 0
G_PRCSS = 0
G_OUTPU = ''

# Globals values
COLORMIN = 253 # Minimal grey color
ERRORMIN = 0.5 # Adjust error for optimization
L_IMAGES = [] # Rows from CSV file
L_CENTRO = [] # Centroids from Pixels image
L_AVG_CE = [] # Centroids from L_CENTRO
L_NEW_PIX = [] # New data Pix after 2nd kmeans

###############################################################################

# MPI Varitables
COMM = MPI.COMM_WORLD
RANK = MPI.COMM_WORLD.Get_rank()
SIZE = MPI.COMM_WORLD.Get_size()

###############################################################################

def show_chart(pixels, centroids):
    """
    Open a new window with chart representation for points and centroids
    """
    plot = GUI_Plot(W_WIDTH, W_HEIGHT, IMG_ROWS, IMG_COLS, SCALE_X, SCALE_Y, pixels, centroids)
    plot.mainloop()

###############################################################################

def show_img(pixels):
    """
    Open a new window with image representation

    :Parameters:
        pixels  : list of greyscale values
    """
    img = GUI_Img(IMG_COLS, IMG_ROWS, 10, 10, pixels)
    img.mainloop()

###############################################################################
###############################################################################

def kmeans_get_distance(px, py, cx, cy):
    """
    Calculate Euclidean distance from point to centroid

    :Parameters:
        px  : x coordinate of the point
        py  : y coordinate of the point
        cx  : x coordinate of the centroid
        cy  : y coordinate of the centroid
    """
    return math.sqrt(math.pow((cy - py), 2) + math.pow((cx - px), 2))

def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [ alist[i*length // wanted_parts: (i+1)*length // wanted_parts]
        for i in range(wanted_parts) ]

###############################################################################
###############################################################################

def kmeans_average():
    """
    Apply kmeans on Centroids Data
    """
    print 'Apply kmeans on Pixels Data: '

    # Init globals variables
    global L_CENTRO
    global L_AVG_CE
    global L_NEW_PIX

    # Init globals
    L_AVG_CE = []
    L_NEW_PIX = []

    # Init variables
    l_new_point = []

    # Convert Centroid to Pixels
    print '\t Convert to simple Pixel point...'
    for c in L_CENTRO:

        # ---------------------------------------------------------------------
        # The next condition will be very dirty : 
        # That's generate a random Runtime Error with NaN warning for 
        # coordinates. In Fact, when you want to show the graph for this 
        # point, TKinter dislike that data and crash. This is because we 
        # handle big data, we can ignore that returned error. But finally, 
        # we MUST fix it... later... maybe...
        # ---------------------------------------------------------------------
        if not math.isnan(c.get_x()) or not math.isnan(c.get_y()):
            l_new_point.append(Pixel(c.get_x(), c.get_y(), 0, 254, G_NUMBR))
        # ---------------------------------------------------------------------

    print '\t Get, compute, optimization centroids...'
    # Init First centroids points
    L_AVG_CE = kmeans_get_centroids(l_new_point)

    # Compute centroids
    l_new_point = kmeans_compute_centroids(l_new_point, L_AVG_CE)

    # Recompute centroid for best solution
    res = kmeans_optimization_centroids(l_new_point, L_AVG_CE)
    L_NEW_PIX = res['pixels']
    L_AVG_CE = res['centroids']

    return

###############################################################################
###############################################################################

def kmeans_optimization_centroids(l_pixels, l_centro):
    """
    Loop to get best centroid for each points.
    """
    # print '\t Centroids optimization...'

    # Init globals
    global ERRORMIN

    # Init list of new cluster
    list_new_c = []

    # Init error values
    new_error = 10000000000000.
    old_error = 0.
    count = 0

    # While absolute value of
    while math.fabs(old_error-new_error) > ERRORMIN :

        # Get the previous error
        old_error = new_error
        new_error = 0

        count += 1

        # Get the average center of each group of cluster
        for c in l_centro:
            
            # Init variables
            avg_x = c.get_x()
            avg_y = c.get_y()
            list_x = []
            list_y = []

            # Compute average of cluster group
            for p in l_pixels:
                if p.get_c() == c.get_i():
                    list_x.append(p.get_x())
                    list_y.append(p.get_y())

            # Compute averages
            avg_x = np.mean(list_x)
            avg_y = np.mean(list_y)

            new_error += kmeans_get_distance(c.get_x(), c.get_y(), avg_x, avg_y)

            # Set new value
            c.set_x(avg_x)
            c.set_y(avg_y)
        
        # Recompute point cluster association
        l_pixels = kmeans_compute_centroids(l_pixels, l_centro)

    # print '\t Optimization, lopping {0} times.'.format(count)
    return {'pixels':l_pixels, 'centroids':l_centro}

###############################################################################

def kmeans_compute_centroids(l_pixels, l_centro):
    """
    Search bests <G_CLUST> centroids for all points
    :Returns:
        l_pixels    : New list of pixels with cluster id
    """

    # Number of point per cluster (minimal)
    c_size = len(l_pixels)//G_CLUST - len(l_pixels)*0.07
    
    # Association cluster id for each data point
    for p in l_pixels:

        # Init list of distance for each point
        list_dist = []
        val_min = 10000000000000

        # Compute distance to centroid
        for c in l_centro:
            dist = kmeans_get_distance(p.get_x(), p.get_y(), c.get_x(), c.get_y())
            # Save the closest centroid
            if dist < val_min:
                val_min = dist
                p.set_c(c.get_i())

        # Count point to centroids
        for c in l_centro:
            if c.get_i() == p.get_c():
                c.set_n(c.get_n()+1)

    return l_pixels

###############################################################################

def kmeans_get_centroids(l_pixels):
    """
    Get <G_CLUST> random points from L_PIXELS
    :Returns:
        l_centroids     : number of centroid for l_pixels
    """

    # Init Globals
    global COLORMIN
    global G_CLUST

    # Clean List of centroids
    l_centroids = []

    # Loop based on number of clusters
    for i in xrange(0, G_CLUST):

        # Get one random point from all pixels
        point = random.choice(l_pixels)

        # While the value isn't a correct color
        while point.get_v() < COLORMIN:
            point = random.choice(l_pixels)
        l_centroids.append(Centroid(point.get_x(), point.get_y(), i))

    # print '\t Number of centroids: {0}'.format(len(l_centroids))
    return l_centroids

###############################################################################

def kmeans_pixels():
    """
    Apply kmeans on Pixels Data
    """
    print 'Apply kmeans on Pixels Data: '

    # Init Globals variables
    global L_CENTRO

    # Basic counter
    count = 0

    # Init returned value from optimization
    res = []

    print '\t Apply get, compute, optimization centroids...'

    # Loop all images from data
    for img in L_IMAGES:

        # Init variables
        l_centro = []
        
        # Init First centroids points
        l_centro = kmeans_get_centroids(img.get_p())

        # Compute centroids
        img.set_p(kmeans_compute_centroids(img.get_p(), l_centro))

        # Recompute centroid for best solution
        res = kmeans_optimization_centroids(img.get_p(), l_centro)
        img.set_p(res['pixels'])
        l_centro = res['centroids']

        for c in l_centro:
            L_CENTRO.append(c)

        count += 1

        if count%500 == 0:
            print '\t Optimization: {0}'.format(count)
    
    print '\t Applied {0} times.'.format(count)
    print
    return

###############################################################################

def kmeans_main():
    """
    Execute successives steps for kmeans algorithm
    """

    # Generate centroid for each image
    kmeans_pixels()

    # Generate centroid for previous centroid for each image
    kmeans_average()

    # Research
    #kmeans_research()

    return
    
###############################################################################

def init_pixels(csv_reader):
    """
    Save all Pixels point from CSV in a global list
    :Parameters:
        csv_reader  : DictReader from csv library
    """
    print 'Init pixels: '

    # Init global
    global IMG_ROWS
    global IMG_COLS
    global COLORMIN
    global L_IMAGES

    # Init a counter
    nb_img = 0
    nb_train = 0

    # Loop in CSV rows
    for row in csv_reader:

        nb_img += 1

        # Get the training value
        l = int(row['label'])

        # Test to get all images of test number
        if l == G_NUMBR: 

            nb_train += 1
            l_pixels = []
            
            # Loop on column informations
            for col in row:

                # If it's a pixel information column
                if not col=='label':

                    # Get Attribute of Pixel
                    p = int(col.split('pixel')[1])
                    x = p%IMG_COLS
                    y = p//IMG_ROWS
                    v = int(row[col])

                    if v > COLORMIN:
                        pixel = Pixel(x, y, p, v, l)
                        l_pixels.append(pixel)

            # Create image object
            img = Image(nb_img, l_pixels, l)

            # Add to data list
            L_IMAGES.append(img)

        # Print State
        if nb_img%10000 == 0 :
            print '\t Row...', nb_img

    print '\t Loaded {0} entries.'.format(nb_img)
    print '\t Loaded {0}/{1} training image.'.format(nb_train, len(L_IMAGES))
    print
    return 

###############################################################################

def load_csv():
    """
    Load CSV file in a dictionnary object and run kmeans_init_point
    """

    # Informative message
    print 'CSV Treatments: '
    print '\t Loading CSV...'

    # Init globals values
    global G_INPUT
    global G_SEPAR

    # Open data file with I/O
    with open(G_INPUT, 'rb') as csvfile:

        data = csv.DictReader(csvfile, delimiter=G_SEPAR)
        print '\t Loading complete.'
        print

        # Init pixels list
        init_pixels(data)

    return

###############################################################################

def main(argv):
    """
    Init call : load application with parameters

    :Parameters:
        -i : inputfile
        -s : separator format
        -n : number [0,9] to test
        -c : number of clusters
        -o : outputfile

    :Example:
        ./main.py -i ../data/train.csv -s "," -n 3 -c 5 -o ../res/
    """

    # Init globals values
    global G_INPUT
    global G_SEPAR
    global G_NUMBR
    global G_CLUST
    global G_PRCSS
    global G_OUTPU

    # Init variables
    data = []

    # Usage
    usage = 'usage: main.py -i <inputfile> -s <separator> -n <number> -c <clusters> -p <process> -o <outputfile>'

    # Try to get parameters
    try:
        opts, args = getopt.getopt(argv,"hi:s:n:c:p:o:",["ifile=","sep=","num=","clust=","proc=","ofile="])
    except getopt.GetoptError:
        print usage
        sys.exit(2)

    # If default values is activate
    if len(opts) == 0:
        G_INPUT = DEF_INPUT
        G_SEPAR = DEF_SEPAR
        G_NUMBR = DEF_NUMBR
        G_CLUST = DEF_CLUST
        G_PRCSS = DEF_PRCSS
        G_OUTPU = DEF_OUTPU

    else:
        # Loop arguments from execution
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                print usage
                sys.exit()
            elif opt in ('-i', '--ifile'):
                G_INPUT = arg
            elif opt in ('-s', '--sep'):
                G_SEPAR = arg
            elif opt in ('-n', '--num'):
                G_NUMBR = int(arg)
            elif opt in ('-c', '--clust'):
                G_CLUST = int(arg)
            elif opt in ('-p', '--proc'):
                G_PRCSS = int(arg)
            elif opt in ('-o', '--ofile'):
                G_OUTPU = arg

    # Inform about arguments
    print 'Parameters:'
    print '\t G_INPUT: ', G_INPUT
    print '\t G_SEPAR: ', G_SEPAR
    print '\t G_NUMBR: ', G_NUMBR
    print '\t G_CLUST: ', G_CLUST
    print '\t G_PRCSS: ', G_PRCSS
    print '\t G_OUTPU: ', G_OUTPU
    print 

    # Loads Pixels from CSV
    load_csv()

    # Run kmeans algorithm
    kmeans_main()

    return

###############################################################################

if __name__ == "__main__":

    # Init timer
    start_time = time.time()

    # Print start
    print '=============== START ==============='

    # Main program
    main(sys.argv[1:])

    # Print end
    print '=============== END ==============='

    # End Timer
    print("%d --- %s seconds ---" % (RANK, time.time() - start_time))
    print

    print "Show plot"
    show_chart(L_NEW_PIX, L_AVG_CE)

