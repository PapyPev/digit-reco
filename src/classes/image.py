#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
The MIT License (MIT)

Copyright (c) 2015 Pierre Vrot
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software 
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS 
IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
AND NONINFRINGEMENT. IN NO EVENT SHALL

THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.

"""

###############################################################################

__author__ = 'pev'
__version__ = "1.0"
__email__ = "pev@gmx.fr"

###############################################################################

class Image(object):
    """
    Simple Point object class
    """

    def __init__(self, i, p, l=[]):
        """
        Constructor for a Point 
        :Attributes:
            i   : Identification Number
            p   : Pixels list
            l   : Label
        """
        self.i = i
        self.p = p
        self.l = l

    def __str__(self):
        """
        String information of a Point 
        """
        return 'Image:: i:{0}, p:{1}, l:{2}'.format(self.i, self.l, self.p)

    def get_i(self):
        """ 
        Get the identification number of an Image 
        :Returns:
            i   : Identification number
        """
        return self.i

    def set_i(self, i):
        """ 
        Set the identification number
        :Parameters:
            i   : Identification number
        """
        self.i = i

    def get_p(self):
        """ 
        Get the list of pixel objects of an Image 
        :Returns:
            p   : List of Pixel objects
        """
        return self.p

    def set_p(self, p):
        """ 
        Set the list of pixel objects
        :Parameters:
            p   : List of pixels objects
        """
        self.p = p

    def get_l(self):
        """ 
        Get the label of an Image 
        :Returns:
            l   : Label value
        """
        return self.l

    def set_l(self, l):
        """ 
        Set the identification number
        :Parameters:
            l   : Label value
        """
        self.l = l
