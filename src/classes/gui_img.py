#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
The MIT License (MIT)

Copyright (c) 2015 Pierre Vrot
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software 
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS 
IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
AND NONINFRINGEMENT. IN NO EVENT SHALL

THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.

"""

###############################################################################

__author__ = 'pev'
__version__ = "1.1"
__email__ = "pev@gmx.fr"

###############################################################################

import Tkinter as tk
import random

###############################################################################

def v2hex(v):
    """
    Convert an decimal value to hexadecimal
    """
    return '{:02x}'.format(v)

###############################################################################

class GUI_Img(tk.Tk):
    """
    Application graphical interface
    """
    def __init__(self, rows, columns, cellwidth, cellheight, pixels, *args, **kwargs):
        """
        Mainloop function

        :Parameters:
            rows    : number of rows
            columns : number of columns
            cellwidth   : with size of cell
            cellheight  : height size of cell
            pixels  : table of greyscale values
        """
        tk.Tk.__init__(self, *args, **kwargs)

        # Determine window size
        win_width = rows*cellwidth
        win_height = columns*cellheight

        # Draw window
        self.canvas = tk.Canvas(self, width=win_width, height=win_height, borderwidth=0, highlightthickness=0)
        self.canvas.pack(side="top", fill="both", expand="true")

        # Init values
        self.rows = rows
        self.columns = columns
        self.cellwidth = cellwidth
        self.cellheight = cellheight

        self.rect = {}

        for column in range(columns):
            for row in range(rows):

                x1 = column*self.cellwidth
                y1 = row * self.cellheight
                x2 = x1 + self.cellwidth
                y2 = y1 + self.cellheight

                color_dec = 0
                for e in pixels:
                    if e.get_x() == column and e.get_y() == row:
                        color_dec = e.get_v()

                color_hexa = v2hex(255-color_dec) # invert: 255-color_dec
                htmlcode = '#'+color_hexa+color_hexa+color_hexa #Greyscale

                self.rect[row,column] = self.canvas.create_rectangle(x1,y1,x2,y2, fill=htmlcode, outline=htmlcode, tags="rect")

