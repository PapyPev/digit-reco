#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
The MIT License (MIT)

Copyright (c) 2015 Pierre Vrot
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software 
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS 
IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
AND NONINFRINGEMENT. IN NO EVENT SHALL

THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.

"""

###############################################################################

__author__ = 'pev'
__version__ = "1.2"
__email__ = "pev@gmx.fr"

###############################################################################

class Point(object):
    """
    Simple Point object class
    """

    def __init__(self, x, y):
        """
        Constructor for a Point 
        :Attributes:
            x   : Coordinate x
            y   : Coordinate y
        """
        self.x = x
        self.y = y

    def __str__(self):
        """
        String information of a Point 
        """
        return "Point:: x:{0}, y:{1}".format(self.x, self.y)

    def get_x(self):
        """ 
        Get the X coordinate of a Point 
        :Returns:
            x   : Coordinate X of the Point object
        """
        return self.x

    def set_x(self, x):
        """ 
        Set the X coordinate of a Point
        :Parameters:
            x   : X Coordinate of the Point
        """
        self.x = x

    def get_y(self):
        """ 
        Get the Y coordinate of a Point 
        :Returns:
            y   : Coordinate Y of the Point object
        """
        return self.y

    def set_y(self, y):
        """ 
        Set the Y coordinate of a Point
        :Parameters:
            y   : Y Coordinate of the Point
        """
        self.y = y

###############################################################################

class Pixel(Point):
    """
    A Pixel is a special Point from CSV sources
    """
    def __init__(self, x, y, p, v, l=None, c=None):
        """
        Constructor for a Pixel Point
        :Attributes:
            x   : Row coordinate
            y   : Column coordinate
            p   : Pixel position (On 28*28px image it's betwin [0,783])
            v   : Value of this pixel
            l   : Label of this pixel
            c   : Cluster value
        """
        self.x = x
        self.y = y
        self.p = p
        self.v = v
        self.l = l
        self.c = c

    def __str__(self):
        """
        String information of a Pixel 
        """
        return "Pixel:: x:{0}, y:{1}, p:{2}, v:{3}, l:{4}, c:{5}".format(self.x, self.y, self.p, self.v, self.l, self.c)

    def get_p(self):
        """ 
        Get the pixel position on image 
        :Returns:
            p   : Pixel position
        """
        return self.p

    def set_p(self, p):
        """ 
        Set the position of the pixel
        :Parameters:
            p   : Pixel position
        """
        self.p = p

    def get_v(self):
        """ 
        Get the value of a pixel 
        :Returns:
            v   : Value of the Pixel (shade of gray)
        """
        return self.v

    def set_v(self, v):
        """ 
        Set the value of a pixel
        :Parameters:
            v   : Value of the Pixel (shade of gray)
        """
        self.v = v

    def get_l(self):
        """ 
        Get the label value of a pixel 
        :Returns:
            l   : Label value
        """
        return self.l

    def set_l(self, l):
        """ 
        Set the label value of a pixel
        :Parameters:
            l   : Label value
        """
        self.l = l

    def get_c(self):
        """ 
        Get the cluster value of a pixel 
        :Returns:
            c   : Cluster number
        """
        return self.c

    def set_c(self, c):
        """ 
        Set the cluster value of a pixel
        :Parameters:
            c   : Cluster number
        """
        self.c = c
        

###############################################################################

class Centroid(Point):
    """
    A Centroid is a special Point from an average of Point
    """
    def __init__(self, x, y, i, n=0):
        """
        Constructor for a Centroid Point
        :Attributes:
            x   : Row coordinate
            y   : Column coordinate
            i   : Identification number
            n   : Number of points per centroid
        """
        self.x = x
        self.y = y
        self.i = i
        self.n = n

    def __str__(self):
        """
        String information of a Pixel 
        """
        return "Centroid:: x:{0}, y:{1}, i:{2}, n:{3}".format(self.x, self.y, self.i, self.n)

    def get_i(self):
        """ 
        Get the centroid identification number 
        :Returns:
            i   : Id of the centroid
        """
        return self.i

    def set_i(self, i):
        """ 
        Set the centroid identification number
        :Parameters:
            i   : Identification number
        """
        self.i = i

    def get_n(self):
        """ 
        Get the number of point for a centroid 
        :Returns:
            n   : Number of point for the centroid
        """
        return self.n

    def set_n(self, n):
        """ 
        Set the number of point for a centroid
        :Parameters:
            n   : Number of point for the centroid
        """
        self.n = n

