#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
The MIT License (MIT)

Copyright (c) 2015 Pierre Vrot
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software 
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS 
IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
AND NONINFRINGEMENT. IN NO EVENT SHALL

THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.

"""

###############################################################################

__author__ = 'pev'
__version__ = "1.1"
__email__ = "pev@gmx.fr"

###############################################################################

import Tkinter as tk
import random
import math
from points import *

###############################################################################

COLORS = ['red', 'blue', 'orange', 'green', 'grey', 'pink', 'cyan', 'magenta', 'yellow', 'maroon']

###############################################################################

def v2hex(v):
    """
    Convert an decimal value to hexadecimal
    """
    return '{:02x}'.format(v)

###############################################################################

class GUI_Plot(tk.Tk):
    """
    Application graphical interface
    """

    # -------------------------------------------------------------------------
    def __init__(self, win_width, win_height, nb_row, nb_col, scale_x, scale_y, points, centroids, *args, **kwargs):
        """
        Mainloop function

        :Parameters:
            win_width   : Window width
            win_height  : Window height
            nb_row      : Number of rows
            nb_col      : Number of col
            scale_x     : Scale for x values
            scale_y     : Scale for y values
            points      : List of DataPoint objects
            centroids   : List of Centroid objects
        """
        tk.Tk.__init__(self, *args, **kwargs)

        # Draw window
        self.canvas = tk.Canvas(self, width=win_width, height=win_height, borderwidth=0, highlightthickness=0)
        self.canvas.pack(side="top", fill="both", expand="true")

        # Scales values
        s_x = win_width//scale_x
        s_y = win_height//scale_y

        # Center point
        c_x = 0
        c_y = win_height

        # Draw points
        for p in points:
            if p.get_v() != 0:
                #print p.get_v()
                p_x = p.get_x()*s_x
                p_y = p.get_y()*s_y
                self.create_circle(p_x, p_y, 1, COLORS[p.get_c()%len(COLORS)], COLORS[p.get_c()%len(COLORS)])

        # Draw centroids
        for ce in centroids:
            ce_x = ce.get_x()*s_x
            ce_y = ce.get_y()*s_y
            text = str(ce.get_i())+'('+str(round(ce.get_x(),1))+','+str(round(ce.get_y(),1))+')'
            self.create_circle(ce_x, ce_y, 3, COLORS[ce.get_i()%len(COLORS)], 'black')
            # self.canvas.create_text(ce_x,ce_y, text=text, fill=COLORS[ce.get_i()%len(COLORS)], anchor='sw')
            self.canvas.create_text(ce_x,ce_y, text=text, fill='black', anchor='sw')

        # Sort centroids
        s = sorted(centroids, key=lambda Centroid: Centroid.y)
        sort = sorted(s, key=lambda Centroid: Centroid.x)

        # Get order point schema
        schema = self.get_schema(sort)

        # Draw schema estimation
        for s in range(len(schema)-1):
            self.canvas.create_line(schema[s].get_x()*s_x, schema[s].get_y()*s_x, schema[s+1].get_x()*s_y, schema[s+1].get_y()*s_y)
        
    # -------------------------------------------------------------------------
    
    def create_circle(self, x, y, r, f, o):
        """
        Draw circle with type

        :Parameters:
            x   : x coordinate
            y   : y coordinate
            r   : radius size
            f   : fill color
            o   : outline color
        """
        self.canvas.create_oval(x-r,y-r,x+r,y+r, fill=f, outline=o)

    # -------------------------------------------------------------------------

    def get_schema(self, l):
        """
        Compute an estimation schema. Return an ordered list of point to draw

        :Parameters:
            l   : list of sorted by (x, y) centroids points
        """

        # Copy of the list
        copy = l

        # Init returned list
        ret_l = []

        # Get the first point (min_x and min_y)
        new_el = copy[0]

        # Add the new element
        ret_l.append(new_el)

        # Loop the copy list
        while len(copy) > 0:
            
            # Revome the last added element from copy list
            copy.remove(new_el)

            # If the length is 1, it's the latest element to add
            if len(copy) == 1:
                
                # Save the new element
                new_el = copy[0]

                # Add the new element to returned list
                ret_l.append(new_el)

            else:

                # Init distance value to other number
                val_min = 100000000
                tmp = new_el

                for c in copy:

                    dist = math.fabs(self.get_distance(new_el.get_x(), new_el.get_y(), c.get_x(), c.get_y()))

                    # Save the closest centroid
                    if dist < val_min:
                        val_min = dist
                        tmp = c

                # Save the new element
                new_el = tmp
                ret_l.append(tmp)

        # for i in ret_l:
        #     print i.get_i()
        return ret_l

    # -------------------------------------------------------------------------

    def get_distance(self, px, py, cx, cy):
        """
        Calculate Euclidean distance from point to centroid

        :Parameters:
            px  : x coordinate of the point
            py  : y coordinate of the point
            cx  : x coordinate of the centroid
            cy  : y coordinate of the centroid
        """
        return math.sqrt(math.pow((cy - py), 2) + math.pow((cx - px), 2))
    

